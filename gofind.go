package main

import (
    "fmt"
    "os"
    "path/filepath"
)

var version = "1.0.0"

func main() {
	
	var dir string
	var filename string

	if len(os.Args) == 1 {
		// The program was not started with arguments.
		fmt.Println("GoFind Version:", version)
		fmt.Println("to use: gofind <filename> <directory>")
		return
	}

	if  len(os.Args) != 0 {
		filename = os.Args[1]
	} else {
		fmt.Println("GoFind Version:", version)
		fmt.Println("gofind <filename> <directory>")
		return
	}

	if len(os.Args) > 2 && len(os.Args[2]) != 0 {
		dir = os.Args[2]
	} else {
		dir = "./"
	}

	// Find all files in the directory recursively that match the filename.
	err := filepath.Walk(dir, func(path string, f os.FileInfo, err error) error {
		if f.Name() == filename {
			fmt.Println(path)
		}
		return nil
	})

	// Check for errors.
	if err != nil {
		fmt.Println(err)
	}

    // Check if the `-v` or `version` argument was passed.
    if len(os.Args) > 1 && (os.Args[1] == "-v" || os.Args[1] == "version") {
        fmt.Println("GoFind Version:", version)
    }
}